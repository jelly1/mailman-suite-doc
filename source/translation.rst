Internationalization(i18n)
==========================

GNU Mailman project uses Weblate_ for translations. If you are interested to
port Mailman to various languages, please head over to Weblate_ and create an
account to get started with translations.

Weblate has very good documentation on how to use it:

    https://docs.weblate.org/en/latest/user/translating.html

Please do not create Merge Requests for translations since it can create merge
conflicts when pulling changes from Weblate and break automation which pulls
and pushes changes between Gitlab_ and Weblate_.

If you have existing translated ``.po`` files, you can reach out to
mailman-developers@python.org.

Integration with Weblate
------------------------

Integration with source control in Gitlab_ and translation project in Weblate_
works using webhooks and some scripts.

Weblate supports webhooks for notifications when there are changes to the
source control. This allows pulling changes to source strings from Gitlab by
adding a webhook notification in Gitlab.

Although Weblate also supports pushing the translations back to the source
control routinely, this hasn't been enabled yet as it would require giving out
push access to the main repository to a 3rd party service.

Mailman maintainers run a routine script that runs every day to pull changes
from Weblate and create Merge Request on the respective projects.


.. _Weblate: https://hosted.weblate.org/projects/gnu-mailman/
.. _Gitlab: https://gitlab.com/mailman/
